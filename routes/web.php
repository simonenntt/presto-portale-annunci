<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index'])->name('home');

// Announcement routes
Route::get('/announcement/create', [AnnouncementController::class, 'create'])->name('announcement.create');
Route::post('/announcement/store', [AnnouncementController::class, 'store'])->name('announcement.store');
Route::get('/announcement/show/{announcement}', [AnnouncementController::class, 'show'])->name('announcement.show');
Route::delete('/announcement/destroy/{announcement}', [AnnouncementController::class, 'destroy'])->name('announcement.destroy');
Route::get('/announcement/history', [AnnouncementController::class, 'announcementHistory'])->name('announcement.history');
Route::get('/announcement/restore/{announcement}', [AnnouncementController::class, 'announcementRestore'])->name('announcement.restore');
Route::delete('/announcement/hard-delete/{announcement}', [AnnouncementController::class, 'announcementHardDelete'])->name('announcement.hardDelete');

// Announcement images routes -Dropzone-
Route::post('/announcement/images/upload', [AnnouncementController::class, 'imagesUpload'])->name('announcement.images.upload');
Route::delete('/announcement/images/remove',[AnnouncementController::class, 'removeImage'])->name('announcement.images.remove');
Route::get('/announcement/images/get',[AnnouncementController::class, 'getImage'])->name('announcement.images.get');