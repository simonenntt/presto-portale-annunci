
<x-layout>

    <x-errorMsg/>

{{-- search BAR  --}}
<section class="container my-5">
    <div>
        <div class="row justify-content-center">
            <div class="col-md-8 central-bar border rounded-3 d-flex justify-content-center py-3 bg-white">
   
                {{-- dropdown  --}}
                <div class="nav-item dropdown mx-5">
                    <a class="nav-link dropdown-toggle text-bigger fw-normal-plus text-dark" href="#" id="dropdown04" data-bs-toggle="dropdown" aria-expanded="false">Categorie</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdown04">
                        @foreach ($categories as $category)
                            <li><a class="dropdown-item text-big" href="#">{{$category->name}}</a></li>                            
                        @endforeach
                      
                      
                    </ul>
                </div>

                {{-- search full text  --}}
                <form class="form-inline my-2 my-md-0 ">
                    <input class="form-control text-bigger" type="text" placeholder="Search">
                </form>

            </div>
        </div>

</section>

{{-- box section  --}}
<section class="container my-5">
        <h3 class="text-center text-uppercase fw-bolder mb-4">Cosa stai cercando?</h3>
        <div class="row center">
            <div class="cat-box bg-danger col-12 col-md-3 d-flex flex-column center rounded-3 py-3">
                <div class="d-flex mt-3 mb-5">
                    <i class="fas fa-angle-double-down fs-2 text-white double-angle"></i>
                    <h3 class="text-white fw-bold text-uppercase">Motori</h3>
                </div>
                <button class="btn btn-outline-light w-100 fw-bolder"> vedi</button>
            </div>
            <div class="cat-box bg-success col-12 col-md-3 d-flex flex-column center rounded-3 py-3">
                <div class="d-flex mt-3 mb-5">
                    <i class="fas fa-angle-double-down fs-2 text-white double-angle"></i>
                    <h3 class="text-white fw-bold text-uppercase">Elettronica</h3>
                </div>
                <button class="btn btn-outline-light w-100 fw-bolder"> vedi</button>
            </div>
            <div class="cat-box bg-info col-12 col-md-3 d-flex flex-column center rounded-3 py-3">
                <div class="d-flex mt-3 mb-5">
                    <i class="fas fa-angle-double-down fs-2 text-white double-angle"></i>
                    <h3 class="text-white fw-bold text-uppercase">Immobili</h3>
                </div>
                <button class="btn btn-outline-light w-100 fw-bolder"> vedi</button>
            </div>
            <div class="cat-box bg-warning col-12 col-md-3 d-flex flex-column center rounded-3 py-3">
                <div class="d-flex mt-3 mb-5">
                    <i class="fas fa-angle-double-down fs-2 text-white double-angle"></i>
                    <h3 class="text-white fw-bold text-uppercase">Shop</h3>
                </div>
                <button class="btn btn-outline-light w-100 fw-bolder"> vedi</button>
            </div>
        </div>

</section>

<section class="container">

    <div class="row">
        <div class="col-12 col-md-4 d-flex flex-column justify-content-center">
            <h3 class="mb-3 fw-bold">Lorem Ipsum</h3>
            <p class="text-bigger">Lorem ipsum dolor sit <strong>cotuset</strong> amet, consectetur <strong>adipisicing</strong>  elit.  <br><a href="" class="text-danger "><u>Scopri...</a></u></p>
            
        </div>

        <div class="col-12 col-md-4 img-box-1 d-flex justify-content-center align-items-end">
            <button class="btn btn-danger mb-3 w-100 fw-bolder">Lorem Ipsum</button>
        </div>

        <div class="col-12 col-md-4 img-box-2 d-flex justify-content-center align-items-end">
            <button class="btn btn-danger mb-3 w-100 fw-bolder">Lorem Ipsum</button>

        </div>
    </div>

</section>



{{-- help  --}}
<section class="container my-5">
   
    <div class="row center">
        <div class="border col-12 rounded-3">

           <div class="row center p-3">
               <div class="col-12 col-md-6 d-flex flex-column justify-content-center">
                    <h2 class="text-left ">lorem</h2>
                   <p class="text-big">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil nostrum dignissimos tenetur id ducimus dicta expedita, corrupti inventore in vel vero rem sed iure adipisci quia aspernatur libero consectetur nesciunt?</p>
                </div>

                <div class="col-12 col-md-5 d-flex center">
                    <a href="{{route('announcement.create')}}">
                        <button class="btn btn-outline-warning text-bigger d-flex center"><i class="fas fa-plus-square mx-3 fs-2"></i>Inserisci annuncio</button>
                    </a>
                </div>
           </div>
        </div>

    </div>

</section>

{{-- last announcement  --}}

<section class="container my-5">
   
    <div class="row center">
        <div class="bg-warning col-3 rounded">
      da
      da
      da
        </div>
        <div class="bg-danger col-9 rounded">

            @foreach ($announcements as $announcement)
                
        <a href="{{route('announcement.show',compact('announcement'))}}">
            <div class="card f-dir-row p-3 my-2">
                
                <div>
                    <img class="card-img-top" src="https://picsum.photos/200" alt="Card image cap">
                </div>
                <div class="card-body justify-content-center align-items-center">
                    <h5 class="card-title">{{$announcement->category->name}}</h5>
                    <h2 class="card-title">{{$announcement->title}}</h2>
                    <p class="card-text mt-3">{{$announcement->body}}</p>
                    <p class="card-text mt-3 fw-bolder">{{$announcement->user->name}}</p>
                    <h5 class="card-title fs-1 fw-bolder text-danger text-end">{{$announcement->price}} €</h5>
                </div>
                <div class="d-flex align-items-end">
                    
                </div>
            </div>

        </a>
            @endforeach

        </div>

        

    </div>

</section>


</x-layout>