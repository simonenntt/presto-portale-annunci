<x-layout>
    <x-errorMsg/>
    <section class='container mt-7'>
        <div class='row'>
            <div class='col-12'>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Price</th>
                            <th scope="col">Cancellato il</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($announcements as $announcement)
                        <tr class="@if($announcement->trashed()) : bg-warning @endif">
                            <th scope="row">{{$announcement->id}}</th>
                            <td>{{$announcement->title}}</td>
                            <td>{{$announcement->price}}</td>
                            <td>{{$announcement->deleted_at}}</td>

                            @if($announcement->deleted_at)
                                <td><a href="{{route('announcement.restore', compact('announcement'))}}"> <button class="btn btn-success">Ripristina</button></a></td>
                            @else
                                <td><button  disabled>Ripristina</button> </td>
                            @endif

                            <td>
                                <form action="{{route('announcement.hardDelete', compact('announcement'))}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Distruggi per sempre</button>
                                </form>
                            </td>
              

                          </tr>
                    @endforeach
                          
                        </tbody>
                      </table>
                  
                </div>
            </div>
    </section>
</x-layout>