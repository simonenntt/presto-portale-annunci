<x-layout>

    <section class="container mt-7">
        <div class="row">
            <div class="col-12 col-md-12">
               <h1 class="text-center">Inserisci il tuo annuncio in pochi minuti</h1> 
                <h3 class="text-center">Vendi online quello che non usi più e comincia a guadagnare</h3>
                <form action="{{ route('announcement.store') }}" method="POST">
                    @csrf
                    
                    <input 
                        type="hidden"
                        name="uniqueSecret"
                        value="{{ $uniqueSecret }}"
                    >

                    <div class="form-group">
                        <h2>DEBBUG {{ $uniqueSecret }}</h2>
                        <label class="mt-5 mb-2 fw-bolder text-bigger" for="fcategory">Seleziona una categoria per il tuo annuncio</label>
                        <select name="category" class="form-select text-bigger" id="fcategory" >
    
                          @foreach ($categories as $category)   
                            <option value="{{ $category->id }}">{{ $category->name }}</option>     
                          @endforeach

                        </select>
        
                      <label class="mt-5 mb-2 fw-bolder text-bigger" for="ftitle">Titolo</label>
                      <input name="title" type="text" class="text-bigger form-control @error('title') is-invalid @enderror" id="ftitle" placeholder="Inserisci il titolo">
                        @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror          
                    </div>

                    <div class="form-group text-bigger">
                        <label class="mt-5 mb-2 fw-bolder" for="fprice">Prezzo</label>
                        <input name="price" type="number" step="0.01" class="text-bigger form-control" id="fprice" placeholder="Inserisci il prezzo">          
                    </div>
                        @error('price')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror   
                    <div class="form-group text-bigger">
                        <label class="mt-5 mb-2 fw-bolder" for="fbody">Descrizione</label>
                        <textarea id="fbody" class="form-control text-bigger" name="body">Inserisci qui il tuo annuncio</textarea>
                        @error('body')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror   
                    </div>

                    <div class="form-group text-bigger">
                        <label class="mt-5 mb-2 fw-bolder" for="images">Inserisci le immaggini dell'oggetto</label>
                        <div class="dropzone" id="drophere"></div>

                        @error('images')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror   
                    </div>


                    <div class="form-check">
                      <input type="checkbox" class="mt-3 mb-2 form-check-input" id="createAnnouncementCheck" required>
                      <label class="mt-3 mb-2 fw-bolder form-check-label" for="createAnnouncementCheck">Dichiaro di aver letto il regolamento per la pubblicazione degli annunci</label>
                    </div>
                      
                    <button type="submit" class=" mt-2 w-100 text-bigger fw-bolder btn btn-outline-warning text-uppercase">Pubblica il tuo annuncio</button>
                </form>
            </div>
        </div>
    </section>




</x-layout>
