<x-layout>

    <section class="container mt-7">
        <div class="row">
            <div class="col-12 col-md-8 mt-5">
                <h4>{{$announcement->category->name}}</h4>
                
                
                <div class="lightbox-gallery">
                    <div class="container">
                        <div class="intro">
                            <h2 class="text-center">Lightbox Gallery</h2>
                            <p class="text-center">Find the lightbox gallery for your project. click on any image to open gallary</p>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6 col-md-2 col-lg-2 item d-flex center"><img class="img-fluid" src="https://picsum.photos/200"></div>
                            <div class="col-sm-6 col-md-2 col-lg-2 item d-flex center"><img class="img-fluid" src="https://picsum.photos/200"></div>
                            <div class="col-sm-6 col-md-2 col-lg-2 item d-flex center"><img class="img-fluid" src="https://picsum.photos/200"></div>
                            <div class="col-sm-6 col-md-2 col-lg-2 item d-flex center"><img class="img-fluid" src="https://picsum.photos/200"></div>
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex justify-content-between">
                                <h1>{{$announcement->title}}</h1>
                                <p class="fs-1 text-danger fw-bolder">{{$announcement->price}} €</p>
                            </div>
                            <p>{{$announcement->body}}</p>
                            
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-4 mt-5">
                <div class='container mt-4'>
                    <div class='row'>
                        <div class='col-12 border rounded-3 p-3 pt-2'>
                        @if(Auth::user() && Auth::user()->name  === $announcement->user->name)
                            <h3> cancella</h3>
                            <div class="col-12 text-end mt-2">
                                <button class="btn btn-danger fw-bolder" data-bs-toggle="modal" data-bs-target="#warningModal">Cancella</button>
                                <a href=""><button class="btn btn-outline-warning fw-bolder">Modifica</button></a>
                            </div>


                            <div class="container">
                                <div class="modal" id="warningModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title text-danger fw-bolder">ATTENZIONE!!</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                            </div>
                                            <div class="modal-body">
                                                <form  method="POST" action="{{route('announcement.destroy', compact('announcement'))}}">
                                                  @csrf
                                                  @method('delete')
                                                  <p class="text-bigger">Sei davvero sicuro di cancellare il tuo annuncio?</p>
                                                  <p>Chiudi la finestra per tornare indietro</p>
                                                  <div class="modal-footer">
                                                    <button type="submit" class="btn btn-danger">Cancella</button>
                                                  </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

    
                  
                        @else
                            <h3>Contatta il proprietario dell'annuncio</h3>
                        @endif
                            <p class="fw-bolder ">{{$announcement->user->name}}</p>
                        @if(Auth::user())
                            <p class="fw-bolder ">{{$announcement->user->email}}</p>
                        @else
                            <p class="fw-bolder ">Accedi per contattare il venditore</p>
                        @endif
                        </div>
                    </div>
                </div>
            </div>

        
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 text-end mt-2">
                    <a href="{{route('home')}}"><button class="btn btn-outline-warning fw-bolder">Torna alla Home</button></a>
                </div>
            </div>
        </div>
    </section>




</x-layout>
