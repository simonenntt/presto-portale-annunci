<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- ICON  --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    
    <title>Document</title>
</head>


    
        <body>
            <div class="bg-body"></div>
            <main class="container">

                <x-navLink/>
                <x-nav/>
                
                
                {{$slot}}
                
                
                
                
            </main>

        {{-- jquery  --}}
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
        <!-- JS -->
        <script src="{{mix('js/app.js')}}"></script>

        {{-- Fontawesome --}}
        <script src="https://kit.fontawesome.com/803844fa40.js" crossorigin="anonymous"></script>

        </body>

</html>
