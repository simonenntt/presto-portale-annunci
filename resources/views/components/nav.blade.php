<hr class="my-0">
<nav class="navbar navbar-expand-md navbar-light bg-transparent text-dark" aria-label="Fourth navbar example">
    <div class="container-fluid">

      <a class="navbar-brand" href="{{route('home')}}">Expand at md</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>



      @if(Auth::user())

      <div>
        <div class="collapse navbar-collapse" id="navbarsExample04">

            
            <ul class="navbar-nav mx-5 mb-2 mb-md-0">
           
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle fw-bolder" href="#" id="dropdown04" data-bs-toggle="dropdown" aria-expanded="false">{{Auth::user()->name}}</a>
                <ul class="dropdown-menu" aria-labelledby="dropdown04">
                  <li><a class="dropdown-item" href="#">Profilo</a></li>
                  <li><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                    getElementById('form-logout').submit();">Logout</a></li>
                  <form method="POST" id="form-logout" action="{{route('logout')}}" style="display: none;">
                    @csrf
                  </form>
    
                  
                </ul>
              </li>
            </ul>
            
            <a href="{{route('announcement.create')}}">
              <button class="btn btn-outline-warning fw-bolder">+ Annuncio</button>
            </a>
            
          </div>
        </div>
        
        @else
        <div class="d-flex">
          <button type="button" class="btn btn-outline-danger mx-2" data-bs-toggle="modal" data-bs-target="#logModal">Login</button>
          <button type="button" class="btn btn-outline-danger mx-2" data-bs-toggle="modal" data-bs-target="#regModal">Registrati</button>
        </div>
    {{-- modal register form  --}}
    <div class="container">
        <div class="modal" id="regModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Registrazione</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class="modal-body">
                        <form  method="POST" action="{{route('register')}}">
                          @csrf
                            <div class="mb-3">
                                <label class="form-label required">Name</label>
                                <input name="name" type="text" class="form-control" value="{{old('name')}}">
                            </div>
                            <div class="mb-3">
                                <label class="form-label required">Email</label>
                                <input name="email" type="email" class="form-control" value="{{old('email')}}">
                            </div>
                            <div class="mb-3">
                                <label class="form-label required">Inserisci la tua Password</label>
                                <input name="password" type="password" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label required">Conferma la tua Password</label>
                                <input name="password_confirmation" type="password" class="form-control">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Registrati</button>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
   {{-- modal login form  --}}
   <div class="container">
    <div class="modal" id="logModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Login</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <form  method="POST" action="{{route('login')}}">
                      @csrf
                        <div class="mb-3">
                            <label class="form-label required">Name</label>
                            <input name="name" type="text" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label class="form-label required">Email</label>
                            <input name="email" type="email" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label class="form-label required">Inserisci la tua Password</label>
                            <input name="password" type="password" class="form-control">
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Accedi</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  
  
  @endif
    </div>
  </nav>
  <hr class="my-0">