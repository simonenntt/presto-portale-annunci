  {{-- error msg --}}

  <div class="mt-5 ">
    @if (session('success'))

    <div class="alert alert-success section d-flex align-items-center justify-content-center">
      <h5 class="text-success">{{(session('success'))}} </h5>
    </div>

    @elseif (session('fail'))
    <div class="alert alert-danger section d-flex align-items-center justify-content-center">
      <h5 class="text-danger">{{(session('fail'))}} </h5>
    </div>

    @endif
  </div>
