<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $categories=[
            0=> ['Abbigliamento', 'Shop', 'Clothing', 'Ropa' , 'Kleidung' ],
            1=> ['Collezionismo', 'Shop','Collecting', 'Coloccionar','Sammeln'],
            2=> ['Smartphone', 'Elettronica' , 'Smartphone', 'Teléfonos inteligente','Smartphone'],
            3=>['Giardinaggio e fai da te', 'Shop','Gardening and DIY', 'Jardinería y bricolaje','Gartenarbeit und Heimwerken'],
            4=> ['Appartamenti', 'Immobili','Apartaments', 'Departamento','Wohnung'],
            5=>['Attività Commerciali','Immobili','Shop','Negocio','Geschäft'],
            6=>['Libri', 'Shop','Books','Libros', 'Buchen'],
            7=>['Auto','Motori','Cars', 'Choche','Wagen'],
            8=>['Musica e strumenti musciali','Shop','Music and Musical Instruments', 'Musica y Instrumentos Musicale','Musik und Musikinstrumente'],
            9=>['Sport','Shop', 'Sport', 'Deporte','Sport'],
            11=>['Moto','Motori','Motorcycle','Motocicleta','Motorrad'],
            12=>['Barche','Motori','Boots','Barcos','Boote'],
            13=>['Capannoni','Immobili','Sheds','Cobertizos','Schuppen'],
            14=>['Tv','Elettronica','Tv','Televisión','Tv'],
            15=>['Audio','Elettronica','Audio','Audio','Audio'],
            16=>['Ville','Immobili','Cottages','Cabañas','Hütten'],
            17=>['Terreni','Immobili','Lands','Tierra','Land'],
           
            
          ];

            foreach($categories as $category)
            {
                DB::table('categories')->insert([
                    'name'=>$category[0],
                    'area'=>$category[1],
                    'en'=>$category[2],
                    'es'=>$category[3],
                    'de'=>$category[4],
                ]);
            };

     
    }
}
