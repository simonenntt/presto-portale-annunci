<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{
   public function index() {

      $announcements = Announcement::orderBy('updated_at', 'DESC')
      ->take(5)
      ->get();

      return view('home',compact('announcements'));
   }
}
