<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AnnouncementRequest;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(Auth::user()){
            $uniqueSecret = $request->old(
                'uniqueSecret',
                base_convert(sha1(uniqid(mt_rand())), 16, 36),
            );
    
            
            return view('announcement.create',compact('uniqueSecret'));
        }else{
            return redirect(route('home'))->with('fail', 'Devi essere loggato!'); 
        }

    
    }

    // Dropzone function 
    public function imagesUpload(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json(
            [
                'id'=> $fileName
            ]
        );
    }

    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }

    public function getImage(Request $request){

        $uniqueSecret =$request->input('uniqueSecret');

        $images =session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images=array_diff($images,$removedImages);

        $data = [];

        foreach($images as $image){
            
            $data[] = [
                'id'=>$image,
                'src'=>Storage::url($image)
            ];
        }
        return response()->json($data);
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request)
    {

        $announcement=Announcement::create([
            'title'=>$request->title,
            'category_id'=>$request->category,
            'price'=>$request->price,
            'body'=>$request->body,
            'user_id'=>Auth::user()->id
        ]);

      /* $uniqueSecret */
      $uniqueSecret = $request->input('uniqueSecret');
      
      $images =  session()->get("images.{$uniqueSecret}", []);
      $removedImages =  session()->get("removedimages.{$uniqueSecret}", []);
   
      $images = array_diff($images, $removedImages);

      foreach($images as $image){


        $i = new AnnouncementImage();

        $fileName = basename($image);
        $newFileName ="public/announcements/{$announcement->id}/{$fileName}";

        Storage::move($image, $newFileName);

        
        $i->file = $newFileName;
        $i->announcement_id = $announcement->id;
        $i->save();
    
    }

      Storage::deleteDirectory(storage_path("app/public/temp/{$uniqueSecret}"));

      return redirect(route('home'))->with('success', 'Hai inserito correttamente il tuo annuncio'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        
        return view('announcement.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        $announcement->delete();

        return redirect(route('home'))->with('success', 'Hai eliminato correttamente il tuo annuncio'); 
    }

    public function announcementHistory(){

        $announcements = Announcement::withTrashed()->get();

        return view('announcement.history', compact('announcements'));
    }

    public function announcementRestore($announcement){
        
        Announcement::withTrashed()->find($announcement)->restore();

        return redirect(route('announcement.history'))->with('success', "Hai ripristinato correttamente l'annuncio");
    }

    public function announcementHardDelete($announcement){

        Announcement::withTrashed()->find($announcement)->forceDelete();

        return redirect(route('announcement.history'))->with('success', "Hai cancellato per sempre l'annuncio");

    }
}
