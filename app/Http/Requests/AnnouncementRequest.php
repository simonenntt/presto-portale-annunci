<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:150',
            'body' => 'required',
            'price' => 'required',
            // 'category'=>'required',
    
        ];
    }

    public function messages()
{
    return [
        'title.required' => 'Il titolo è un campo obbligatorio!',
        'title.max' => 'Il titolo può contenere un massimo di 150 caratteri!',
        'body.required' => "la Descrizione dell'annuncio è un campo obbligatorio!",
        // 'category.required' => "la Categoria dell'annuncio è un campo obbligatorio!",
    ];
}
}
